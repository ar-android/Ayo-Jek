package com.ahmadrosid.ayo_jek.services;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by ocittwo on 6/11/16.
 */
public class FcmServices extends FirebaseMessagingService{

    private static final String TAG = "FcmServices";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        log(remoteMessage.getData().toString());
    }

    /**
     * Logging
     * @param s
     */
    void log(String s){
        Log.d(TAG, "log: " + s);
    }
}
